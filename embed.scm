;; Example 1
;;(c-declare #<<c-declare-end
;;#include <stdio.h>
;;c-declare-end
;;
;;)
;;
;;((c-lambda () void
;;#<<c-lambda-end
;;printf("Hello World!\n");
;;c-lambda-end
;;))

Example 2
(c-declare #<<c-declare-end

  int square(int x) {
    return x * x;
  }

c-declare-end
)
;; Example 2
;; Define a Scheme function to call 'square'
;;(define square-scheme (c-lambda (int) int "square"))
;;(display "Enter an integer: ")
;;(define input (read))
;;(let ((result (square-scheme input)))
;;  (display "The square of ")
;;  (display input)
;;  (display " is ")
;;  (display result)
;;  (newline))

;; Example 3
(c-declare #<<c-declare-end
  #include "triple.c"
c-declare-end
)

(define num 100)
(define triple (c-lambda (int) int "triple"))

(display (string-append "The triple of " (number->string num) " is " (number->string (triple num))))
(newline)
