#include <stdio.h>

#include "gambit.h"

#include "read.h"

#define SCHEME_LIBRARY_LINKER ___LNK_read__

___BEGIN_C_LINKAGE
extern ___mod_or_lnk SCHEME_LIBRARY_LINKER (___global_state_struct*);
___END_C_LINKAGE


int main(int argc, char** argv) {
  printf("Hello World, this is from C\n");

  ___setup_params_struct setup_params;
  ___setup_params_reset (&setup_params);

  setup_params.version = ___VERSION;
  setup_params.linker = SCHEME_LIBRARY_LINKER;

  ___setup (&setup_params);

  char *text = c_read_file_into_string("dickinson.txt");
  printf("%s\n", text);

  ___cleanup();
  
  return 0;
}
