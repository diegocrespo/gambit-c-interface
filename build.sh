#!/bin/bash
gsc -c read.scm
gsc -link read.c
gsc -obj -cc-options -D___LIBRARY read.c read_.c
gsc -obj main.c
gcc read.o read_.o main.o -I/usr/include/gambit.h /usr/lib/x86_64-linux-gnu/libgambit.so.4 -lm -ldl -lutil -lssl -lcrypto -o main
