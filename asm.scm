
((c-lambda () void
#<<c-lambda-end
    int a = 10;
    int b = 20;
    int result;

    // Inline assembly block
    __asm__(
        "add %1, %2\n\t" // Assembly instruction: Add b to a
        "mov %2, %0\n\t" // Move the sum (now in a) to result
        : "=r" (result)   // Output operands: 'result' is output in a register
        : "r" (a), "r" (b) // Input operands: a and b are inputs, both in registers
    );

    printf("The result is: %d\n", result);

c-lambda-end
))

(display "ASM example")
(newline)
