(c-declare #<<c-declare-end

#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#define STACK_SIZE 30000

struct Interpreter {
    unsigned char stack[STACK_SIZE];
    unsigned char *stack_ptr;
    char *code;
    char *code_ptr;
    bool is_valid;
};


struct Interpreter createInterpreter(const char code[]);

int interpreterInterpret(struct Interpreter *interpreter);


struct Interpreter createInterpreter(const char code[]) {
    struct Interpreter interpreter;
    interpreter.is_valid = false;
    // Initialize stack to zero
    memset(interpreter.stack, 0, STACK_SIZE);

    // Set the data pointer to the beginning of the stack
    interpreter.stack_ptr = interpreter.stack;

    // Allocate memory and copy the Brainfuck code for the interpreter
    // Ensure the code is null-terminated
    interpreter.code = strdup(code);
    if (interpreter.code == NULL) {
        perror("Failed to copy code to interpreter");
        return interpreter;
    }

    // Set the code pointer to the beginning of the code.
    interpreter.code_ptr = interpreter.code;
    interpreter.is_valid = true;

    return interpreter;
}

int interpreterInterpret(struct Interpreter *interpreter) {
    printf("===BEGINNING INTERPRETATION===\n");

    int user_number;
    char *code = interpreter->code;
    for (int i = 0; i < strlen(code); i++) {
        char instr = code[i];
        switch (instr) {
            case '>':
                interpreter->stack_ptr++;
                break;
            case '<':
                interpreter->stack_ptr--;
                break;
            case '+':
                (*(interpreter->stack_ptr))++;
                break;
            case '-':
                (*(interpreter->stack_ptr))--;
                break;
            case '.':
                // Output the byte at the data pointer
                printf("%c", *(interpreter->stack_ptr));
                break;
            case ',':
                // Loop indefinitely until the user provides a valid input
                // What do I do if a user enters a letter or a number greater than int max?
                while (1) {
                    printf("Please enter a number between 0 and 255: ");

                    // Read an integer from the user
                    scanf("%d", &user_number);

                    // Check if the number is within the specified range
                    if (user_number >= 0 && user_number <= 255) {
                        // Break the loop if the number is valid
                        break;
                    } else {
                        // Inform the user of the invalid input
                        printf("Invalid input. Please enter a number between 0 and 255: ");
                    }
                }
                printf("You entered: %d\n", user_number);
                (*(interpreter->stack_ptr)) = (char) user_number;

                break;
            case '[':
                if (*(interpreter->stack_ptr) == 0) {
                    int loop = 1;
                    while (loop > 0){
                        ++i;
                       if (code[i] == '['){
                           ++loop;
                       }
                       if (code[i] == ']'){
                           --loop;
                       }
                    }
                }
                    break;
            case ']':
                if (*(interpreter->stack_ptr) != 0) {
                    int loop = 1;
                    while (loop > 0){
                        --i;
                        if (code[i] == '['){
                            --loop;
                        }
                        if (code[i] == ']'){
                            ++loop;
                        }
                    }
                }
                break;
            default:
                fprintf(stderr, "The value %c is not a valid instruction\n", code[i]);
                return EXIT_FAILURE;
        }

    }
    printf("\n");
    printf("===ENDING INTERPRETATION===\n");
    return EXIT_SUCCESS;
}

char *readCode(const char file_name[]) {
    FILE *file;
    int file_size = 10000; // Initial size for the content buffer
    char *content = malloc(file_size * sizeof(char)); // Dynamically allocate memory for the content
    if (content == NULL) {
        perror("Failed to allocate memory");
        return NULL;
    }

    int index = 0; // Index to keep track of the position in the array

    // Open the file in read mode
    file = fopen(file_name, "r");
    if (file == NULL) {
        fprintf(stderr, "Error opening file %s\n", file_name);
        free(content); // Free the allocated memory before returning
        return NULL;
    }

    int ch;
    while ((ch = fgetc(file)) != EOF) {
        if (index >= file_size - 1) {
            // Check if buffer needs to be expanded
            file_size += 10000; // Increase buffer size
            char *new_content = realloc(content, file_size);
            if (new_content == NULL) {
                perror("Failed to reallocate memory");
                free(content); // Free the original block
                fclose(file);
                return NULL;
            }
            content = new_content;
        }
        content[index] = (char) ch;
        ++index;
    }
    content[index] = '\0'; // Null-terminate the string
    fclose(file);
    return content;
}
c-declare-end

)

((c-lambda () void
#<<c-lambda-end
    char filename[] = "instructions_hello_world.txt";
    char *content = readCode(filename);
    printf("%s\n", content);
    struct Interpreter bf = createInterpreter(content);
    interpreterInterpret(&bf);

c-lambda-end
))
