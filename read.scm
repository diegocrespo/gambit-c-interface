(c-define (read-file-into-string filename) (char-string) char-string "c_read_file_into_string" ""
  ;; Body of the Scheme procedure
  (let ((port (open-input-file filename))  ;; Open the file
        (content ""))
    (let loop ((line (read-line port)))  ;; Read line by line
      (if (eof-object? line)  ;; Check if end of file is reached
          (begin
            (close-input-port port)  ;; Close the file
            content)  ;; Return the content accumulated
          (begin
            (set! content (string-append content line "\n"))  ;; Append line to content
            (loop (read-line port)))))  ;; Continue looping
    ))
