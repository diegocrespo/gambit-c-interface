(include "structs.scm")
(include "enums.scm")
(c-declare #<<c-declare-end
#include "raylib.h"
c-declare-end
)
;; Define the Color struct as a foreign type

(define screenWidth 800)
(define screenHeight 450)

;; Declare Raylib functions
;; Modify the make-color function to create a Color instance

(define myRect (make-rectangle 100.0 100.0 100.0 100.0))

(define InitWindow (c-lambda (int int char-string) void "InitWindow"))
(define SetTargetFPS (c-lambda (int) void "SetTargetFPS"))
(define WindowShouldClose (c-lambda () bool "WindowShouldClose"))
(define BeginDrawing (c-lambda () void "BeginDrawing"))
(define EndDrawing (c-lambda () void "EndDrawing"))
(define ClearBackground (c-lambda (color) void "ClearBackground"))
(define CloseWindow (c-lambda () void "CloseWindow"))
(define DrawText (c-lambda (char-string int int int color) void "DrawText"))
(define DrawRectangle (c-lambda (int int int int color) void "DrawRectangle"))
(define DrawRectangleV (c-lambda (vector2 vector2 color) void "DrawRectangleV"))

(define IsKeyPressed (c-lambda (int) bool "IsKeyPressed"))
(define IsKeyPressedRepeat (c-lambda (int) bool "IsKeyPressedRepeat"))
(define IsKeyDown (c-lambda (int) bool "IsKeyDown"))
(define IsKeyReleased (c-lambda (int) bool "IsKeyReleased"))
(define IsKeyUp (c-lambda (int) bool "IsKeyUp"))

;; Initialization
(InitWindow screenWidth screenHeight "raylib [core] example - basic window")
(SetTargetFPS 60)

(let loop ()
  (unless (WindowShouldClose)
    (BeginDrawing)
    (ClearBackground ORANGE)
    ;;(DrawText "Hello World" 100 100 120 LIGHTGRAY)
    (DrawRectangle 20 20 20 20 RAYWHITE)
    (if (IsKeyDown KEY_SPACE)
	(DrawText "SPACE" 100 100 120 LIGHTGRAY)
	(DrawText "NO SPACE" 100 100 120 LIGHTGRAY))
    (EndDrawing)
    (loop)))

(CloseWindow)
