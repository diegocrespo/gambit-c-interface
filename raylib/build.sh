#!/bin/bash
echo "Compiling C file and linking with Raylib..."
gsc -exe -cc-options "-Iinclude/ -Llib/" -ld-options "-Llib/ -lraylib -lGL -lm -lpthread -ldl -lrt -lX11" main.scm
