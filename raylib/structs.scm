(c-declare #<<c-declare-end
#include "raylib.h"
c-declare-end
)

(c-define-type vector2 (struct "Vector2"))
(c-define-type color (struct "Color"))
(c-define-type rectangle (struct "Rectangle"))

(define make-color (c-lambda (unsigned-int8 unsigned-int8 unsigned-int8 unsigned-int8) color
#<<c-lambda-end
Color col =(Color){___arg1, ___arg2, ___arg3, ___arg4}; 
___return( col);
c-lambda-end
))

(define make-rectangle (c-lambda (float float float float) rectangle
#<<c-lambda-end
Rectangle rec =(Rectangle){___arg1, ___arg2, ___arg3, ___arg4}; 
___return(rec);
c-lambda-end
))


(define LIGHTGRAY  (make-color 200 200 200 255))
(define GRAY       (make-color 130 130 130 255))
(define DARKGRAY   (make-color 80 80 80 255 )) 
(define YELLOW     (make-color 253 249 0 255 ))
(define GOLD       (make-color 255 203 0 255 ))
(define ORANGE     (make-color 255 161 0 255 ))
(define PINK       (make-color 255 109 194 255))
(define RED        (make-color 230 41 55 255 ))
(define MAROON     (make-color 190 33 55 255 ))
(define GREEN      (make-color 0 228 48 255 )) 
(define LIME       (make-color 0 158 47 255 )) 
(define DARKGREEN  (make-color 0 117 44 255 )) 
(define SKYBLUE    (make-color 102 191 255 255))
(define BLUE       (make-color 0 121 241 255 ))
(define DARKBLUE   (make-color 0 82 172 255 )) 
(define PURPLE     (make-color 200 122 255 255))
(define VIOLET     (make-color 135 60 190 255))
(define DARKPURPLE (make-color 112 31 126 255))
(define BEIGE      (make-color 211 176 131 255))
(define BROWN      (make-color 127 106 79 255))
(define DARKBROWN  (make-color 76 63 47 255)) 

(define WHITE      (make-color 255 255 255 255))
(define BLACK      (make-color 0 0 0 255))    
(define BLANK      (make-color 0 0 0 0))      
(define MAGENTA    (make-color 255 0 255 255))
(define RAYWHITE   (make-color 245 245 245 255))
