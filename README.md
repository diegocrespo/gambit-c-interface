# DeusInMachina.net Blog Code

Hi! This is the code from [an article](https://www.deusinmachina.net/p/gambit-c-scheme-and-c-a-match-made) I wrote on [DeusInMachina.net](https://deusinmachina.net) about Gambit-C!

## License
This project is licensed under the Apache License 2.0 - see the [LICENSE](LICENSE) file for details.
